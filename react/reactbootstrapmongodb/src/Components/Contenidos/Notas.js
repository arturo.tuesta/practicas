import React, { Component } from 'react';

class Notas extends Component {
    constructor(){
        super()
        this.eliminarContenido = this.eliminarContenido.bind(this);
    }
    
    eliminarContenido(contenidoId){
        this.props.eliminarContenido(contenidoId);
    }
    
    render()
    { 
        const { notas } = this.props;
        return (
                <div>
                {notas.map((nota, key) => 
                    <div key={ key } className="card text-center mt-4" >
                        <div className="card-header">
                            usuario: { nota.notaUser }
                        </div>
                        <div className="card-body">
                            <h5 className="card-title">{ nota.notaContent }</h5>
                            <button type="button" onClick={() => this.eliminarContenido(nota._id)} className="btn btn-primary">Eliminar</button>
                        </div>
                    </div>                
                )}
                </div>
            )
    }
}

export default Notas;
