import React, { Component } from 'react';

class Notas extends Component {
    constructor() {
        super()
        this.agregarContenido = this.agregarContenido.bind(this);
    }
    
    agregarContenido(){
        let nota = {};
        let tm = this.textMensaje.value;
        let tu = this.textUsuario.value;
        if(tm !== "" && tu !== ""){
            nota = {
               notaContent: tm,
               notaUser: tu
            };
            this.textMensaje.value = "";
            this.textUsuario.disabled = true;
            this.props.agregarContenido(nota);
        }
        
        this.textMensaje.focus();
    }

    render() {
        return (
                <div className="cajita">
                    <form className="text-center border border-light p-2">
                        <div className="form-group">
                            <input type="text" autoComplete="off" ref={(input) => this.textMensaje = input} className="form-control" name="textMensaje" id="textMensaje" aria-describedby="emailHelp" placeholder="Ingresa un mensaje" />
                        </div>
                        <div className="form-group">
                            <input type="text" autoComplete="off" ref={(input) => this.textUsuario = input} className="form-control" name="textUsuario" id="textUsuario" aria-describedby="emailHelp" placeholder="Ingresa tu nombre" />
                        </div>
                        <button type="button" onClick={() => this.agregarContenido()}  className="btn btn-primary">Guardar</button>
                    </form>
                </div>
                );
    }
}

export default Notas;
