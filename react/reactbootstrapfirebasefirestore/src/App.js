import React, { Component } from 'react';
import logo from './logo.svg';
import Formulario from './Components/Formularios/Notas.js';
import Contenido from './Components/Contenidos/Notas.js';
import firebase from './Config/FirebaseFirestore.js';
import './App.css';

var database = firebase.firestore();

var notasColeccion = database.collection("notas");

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notas: []
        }
        this.eliminarContenido = this.eliminarContenido.bind(this);
        this.agregarContenido = this.agregarContenido.bind(this);
    }

    agregarContenido(nota) {
        nota.timestamp =  firebase.firestore.FieldValue.serverTimestamp();
        console.log(nota);
        notasColeccion.add(nota).then(() => {
            console.log("Documento Agregado");
        }).catch((error) => {
            console.error("Algo paso al agregar: ", error);
        })
    }

    eliminarContenido(notaId) {
        notasColeccion.doc(notaId).delete().then(() => {
            console.log("Documento eliminado");
        }).catch((error) => {
            console.error("Algo paso al elimina: ", error);
        });
    }

    componentDidMount() {
        notasColeccion.orderBy("timestamp", "desc").onSnapshot((listado) => {
            console.log("ENTRO A componentDidMount value");
            var notas = [];
            listado.forEach(function (objeto) {
                var nota = objeto.data();
                notas.push({
                    notaId: objeto.id,
                    notaContent: nota.notaContent,
                    notaUser: nota.notaUser
                })
            });
            this.setState({notas});
        });
    }
    
    onCollectionUpdate = (querySnapshot) => {
        const boards = [];
        querySnapshot.forEach((doc) => {
            const {title, description, author} = doc.data();
            boards.push({
                key: doc.id,
                doc, // DocumentSnapshot
                title,
                description,
                author,
            });
        });
        this.setState({
            boards
        });
    }

    render() {
        return (
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                    </header>
                    <div className = "notesContainer" >
                        <div className="notesHeader">
                            <h5 className="text-white">Pruebas con reatc js y firebase (firestore)</h5>
                        </div>
                        <div className="notesBoby">
                            <div className="notesBoby">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <Formulario agregarContenido={this.agregarContenido} />
                                    </div>
                                    <div className="col-sm-8">
                                        <Contenido eliminarContenido={this.eliminarContenido} notas={this.state.notas} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                );
    }
}

export default App;
