import * as firebase from 'firebase/app';
import 'firebase/database';

var config = {
    apiKey: "AIzaSyAK0UyNSm8v1oxydXsB51F1vN1WCAjmUQw",
    authDomain: "lokydemo.firebaseapp.com",
    databaseURL: "https://lokydemo.firebaseio.com",
    projectId: "lokydemo",
    storageBucket: "lokydemo.appspot.com",
    messagingSenderId: "1082926390774"
};

firebase.initializeApp(config);

export default firebase;