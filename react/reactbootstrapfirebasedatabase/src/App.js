import React, { Component } from 'react';
import logo from './logo.svg';
import Formulario from './Components/Formularios/Notas.js';
import Contenido from './Components/Contenidos/Notas.js';
import firebase from './Config/FirebaseDatabase.js';
import './App.css';

var database = firebase.database();
var notasColeccion = database.ref().child('notas');

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notas: []
        }
        this.eliminarContenido = this.eliminarContenido.bind(this);
        this.agregarContenido = this.agregarContenido.bind(this);
    }

    agregarContenido(nota) {
        nota.timestamp = firebase.database.ServerValue.TIMESTAMP
        notasColeccion.push(nota);
    }

    eliminarContenido(notaId) {
        notasColeccion.child(notaId).remove();
    }

    componentDidMount() {
        notasColeccion.orderByChild("timestamp").on('value', (listado) => {
            console.log("ENTRO A componentDidMount value");
            var notas = [];
            listado.forEach(function (objeto) {
                var nota = objeto.val();
                notas.push({
                    notaId: objeto.key,
                    notaContent: nota.notaContent,
                    notaUser: nota.notaUser
                })
            });
            notas.reverse();
            this.setState({notas});
        });
    }

    render() {
        return (
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                    </header>
                    <div className = "notesContainer" >
                        <div className="notesHeader">
                            <h5 className="text-white">Pruebas con reatc js y firebase (database)</h5>
                        </div>
                        <div className="notesBoby">
                            <div className="notesBoby">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <Formulario agregarContenido={this.agregarContenido} />
                                    </div>
                                    <div className="col-sm-8">
                                        <Contenido eliminarContenido={this.eliminarContenido} notas={this.state.notas} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                );
    }
}

export default App;
