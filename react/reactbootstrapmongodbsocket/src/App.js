import React, { Component } from 'react';
import logo from './logo.svg';
import Formulario from './Components/Formularios/Notas.js';
import Contenido from './Components/Contenidos/Notas.js';
import axios from 'axios';
import openSocket from 'socket.io-client';

import './App.css';



const socket = openSocket('http://192.168.85.138:4001');



class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notas: []
        }
        this.eliminarContenido = this.eliminarContenido.bind(this);
        this.agregarContenido = this.agregarContenido.bind(this);
    }

    emitirMensaje() {
        console.log("LLego aca al emit");
        socket.emit('loky', this.state.color)
    }

    agregarContenido(nota) {
        fetch('http://192.168.85.138:4001/apis/notas/putData', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                notaId: 1,
                notaContent: nota.notaContent,
                notaUser: nota.notaUser
            })
        }).then(res => {
            //this.componentDidMount();
            this.emitirMensaje();
        });

    }
    
    eliminarContenido(notaId) {
        axios.delete('http://192.168.85.138:4001/apis/notas/deleteData', {data: {"notaId": notaId}})
                .then(res => {
                    //this.componentDidMount();
                    this.emitirMensaje();
                });

    }

    componentDidMount() {
        axios.get('http://192.168.85.138:4001/apis/notas/')
                .then(res => {
                    let notas = res.data.data;
                    this.setState({notas});
                });
    }

    render() {
        socket.on('loky_dice_reload', (mensaje) => {
            this.componentDidMount();
        })

        return (
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                    </header>
                    <div className = "notesContainer" >
                        <div className="notesHeader">
                            <h5 className="text-white">Pruebas con reatc js y firebase (firestore)</h5>
                        </div>
                        <div className="notesBoby">
                            <div className="notesBoby">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <Formulario agregarContenido={this.agregarContenido} />
                                    </div>
                                    <div className="col-sm-8">
                                        <Contenido eliminarContenido={this.eliminarContenido} notas={this.state.notas} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                );
    }
}

export default App;
