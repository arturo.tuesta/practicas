const webpack = require('webpack');

module.exports = {
  configureWebpack: {
      devServer: {
        host: '0.0.0.0',
        hot: true,
        port: 5000,
        disableHostCheck: true
      }
  }
}
