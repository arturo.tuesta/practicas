// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const NotasSchema = new Schema(
  {
    notaId: Number,
    notaContent: String,
    timestamp: { type : Date, default: Date.now }
  },
  { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("notas", NotasSchema);
