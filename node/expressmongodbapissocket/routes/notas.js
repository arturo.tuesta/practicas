var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var express = require('express');
var router = express.Router();
var Notas = require("./../models/Notas");

var dbRoute = "mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky"

mongoose.connect(
  dbRoute,
  { useNewUrlParser: true }
);

var db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));
db.on("error", console.error.bind(console, "MongoDB connection error:"));

router.get("/", (req, res) => {
  Notas.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  }).sort({timestamp: 'desc'});
});

router.get("/getData", (req, res) => {
  Notas.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  }).sort({timestamp: 'desc'});
});

router.post("/updateData", (req, res) => {
  const { id, update } = req.body;
  Notas.findOneAndUpdate(id, update, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.delete("/deleteData", (req, res) => {
  const { notaId } = req.body;
  Notas.findOneAndDelete({"_id": notaId}, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

router.post("/putData", (req, res) => {
  let data = new Notas();

  const { notaId, notaContent, notaUser } = req.body;

  if ((!notaId && notaId !== 0) || !notaContent) {
    return res.json({
      success: false,
      error: "INVALID INPUTS"
    });
  }
  data.notaContent = notaContent;
  data.notaId = notaId;
  data.notaUser = notaUser;
  data.save(err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

module.exports = router;
