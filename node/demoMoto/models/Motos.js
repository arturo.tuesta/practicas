// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const MotosSchema = new Schema(
  {
    nombre: String,
    modelo: String,
    color: String,
    cilindrada: String,
    precio: String,
    timestamp: String
  },
  { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("motos", MotosSchema);
