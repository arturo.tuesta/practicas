var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var express = require('express');
var router = express.Router();
var Motos = require("./../../models/Motos");
var dbRoute = "mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky"

mongoose.connect(
        dbRoute,
        {useNewUrlParser: true}
);

var db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));
db.on("error", console.error.bind(console, "MongoDB connection error:"));

router.get("/", (req, res) => {
    Motos.find((err, data) => {
        if (err)
            return res.json({success: false, error: err});
        return res.render('backend/motos/index.hbs', {data: data});
    }).sort({timestamp: 'desc'});
});

router.get("/agregar", (req, res) => {
    Motos.find((err, data) => {
        if (err)
            return res.json({success: false, error: err});
        return res.render('backend/motos/agregar.hbs', {data: data});
    }).sort({timestamp: 'desc'});
});

router.post("/agregar", (req, res) => {
    let data = new Motos();
    const {nombre, modelo, color, cilindrada, precio} = req.body;

    if (nombre === "") {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }
    data.nombre = nombre;
    data.modelo = modelo;
    data.color = color;
    data.cilindrada = cilindrada;
    data.precio = precio;
    data.save(err => {
        if (err)
            return res.json({success: false, error: err});
        return res.redirect('/backend/motos/');
    });    
});

router.get("/editar/:id", (req, res) => {
    var contentId = req.params.id;
    Motos.findById(contentId, (err, data) => {
        if (err)
            return res.json({success: false, error: err});
        return res.render('backend/motos/editar.hbs', {data: data});
    }).sort({timestamp: 'desc'});
});

router.post("/editar/:id", (req, res) => {
    var id = req.params.id
    const {nombre, modelo, color, cilindrada, precio} = req.body;
    var update = { 
        nombre: nombre,
        modelo: modelo,
        color: color,
        cilindrada: cilindrada,
        precio: precio
    }
    Motos.findOneAndUpdate(id, update, err => {
        if (err)
            return res.json({success: false, error: err});
        return res.redirect('/backend/motos/');
    });
});

router.get("/eliminar/:id", (req, res) => {
    var id = req.params.id;
    Motos.findOneAndDelete({"_id": id}, err => {
        if (err)
            return res.send(err);
        return res.redirect('/backend/motos/');
    });
});

module.exports = router;
