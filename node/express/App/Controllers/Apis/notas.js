var mongoose = require("mongoose");
var express = require('express');
var router = express.Router();
var Notas = require("./../../models/Notas");

var notas = {
    conectar() {
        var dbRoute = "mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky"
        mongoose.connect(
                dbRoute,
                {useNewUrlParser: true}
        );
        var db = mongoose.connection;
        db.once("open", () => console.log("connected to the database"));
        db.on("error", console.error.bind(console, "MongoDB connection error:"));
    },
    buscarContenidos() {
        notas.conectar();
        return Notas.find((err, data) => {
            if (err)
                return {success: false, error: err};
            return {success: true, data: data};
        }).sort({timestamp: 'desc'});
    },
    agregarContenido(req) {
        let data = new Notas();

        const {notaId, notaContent, notaUser} = req.body;

        if ((!notaId && notaId !== 0) || !notaContent) {
            return res.json({
                success: false,
                error: "INVALID INPUTS"
            });
        }
        data.notaContent = notaContent;
        data.notaId = notaId;
        data.notaUser = notaUser;
        data.save(err => {
            if (err)
                return {success: false, error: err};
            return {success: true};
        });
    },
    eliminarContenido(req) {
        notas.conectar();
        const {notaId} = req.body;
        return Notas.findOneAndDelete({"_id": notaId}, err => {
            if (err)
                return {success: false, error: err};
            return {success: true};
        });
    }
}

module.exports = notas;
