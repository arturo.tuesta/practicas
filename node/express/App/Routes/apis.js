var express = require('express');
var router = express.Router();
var notas = require('./../Controllers/Apis/notas');

/* GET home page. */
router.get('/notas', function (req, res, next) {
    notas.buscarContenidos().then((data) => {
        return res.json({success: true, data: data});
        ;
    });
});

router.post('/notas/putData', function (req, res, next) {
    notas.agregarContenido(req);
    return res.json({success: true});
});

router.delete('/notas/deleteData', function (req, res, next) {
    notas.eliminarContenido(req).then((data) => {
        return res.json({success: true});
        ;
    });
});



module.exports = router;
