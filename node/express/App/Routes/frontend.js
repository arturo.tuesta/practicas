
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('Frontend/index', { title: 'Express' });
});

router.get('/firebase-database', function(req, res, next) {
  res.render('Frontend/Genericos/firebasedatabase', { title: 'Express' });
});

router.get('/firebase-firestore', function(req, res, next) {
  res.render('Frontend/Genericos/firebasefirestore', { title: 'Express' });
});

router.get('/mongodb', function(req, res, next) {
  res.render('Frontend/Genericos/mongodb', { title: 'Express' });
});

router.get('/mongodb-socket', function(req, res, next) {
  res.render('Frontend/Genericos/mongodbsocket', { title: 'Express' });
});
module.exports = router;
