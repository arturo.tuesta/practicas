<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
function coleccion($coleccion) {
    $cliente = new \MongoDB\Client("mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky", [],
            [
        'typeMap' => [
            'array' => 'array',
            'document' => 'array',
            'root' => 'array',
        ],
    ]);
    $options = [
        'sort' => ['timestamp' => -1]
        , 'limit' => 10
    ];
    return $cliente->loky->{$coleccion};
}

$app->get('/', function (Request $request, Response $response, array $args) {
    $this->logger->info("Slim-Skeleton '/' route");
    return $this->renderer->render($response, 'Frontend/index.phtml', $args);
});

$app->get('/firebase-database', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'Frontend/firebase_database.phtml', $args);
});

$app->get('/firebase-firestore', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'Frontend/firebase_firestore.phtml', $args);
});

$app->get('/mongodb', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'Frontend/mongodb.phtml', $args);
});

$app->get('/mongodb-socket', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'Frontend/mongodb_socket.phtml', $args);
});


$app->get('/apis/notas/', function (Request $request, Response $response, array $args) {
    $options = [
        'sort' => ['timestamp' => -1]
        , 'limit' => 10
    ];
    $filter = [];
    $list = coleccion("notas")->find($filter, $options)->toArray();
    $resultado["hash"] = md5(json_encode($list));
    $resultado["list"] = $list;
    return $response->withJson($resultado);
});

$app->delete('/apis/notas/deleteData', function (Request $request, Response $response, array $args) {
    $allPostPutVars = $request->getParsedBody();
    $notaId = $allPostPutVars["notaId"];
    $insertOneResult = coleccion("notas")->deleteOne(["_id" => new \MongoDB\BSON\ObjectId($notaId)]);
    return $response->withJson(["status" => ($insertOneResult->getDeletedCount() < 1 ? "ERROR" : "OK")]);
});

$app->post('/apis/notas/putData', function (Request $request, Response $response, array $args) {
    $allPostPutVars = $request->getParsedBody();
    $nota["notaId"] = 1;
    $nota["notaContent"] = $allPostPutVars["notaContent"];
    $nota["notaUser"] = $allPostPutVars["notaUser"];
    $nota["timestamp"] = new \MongoDB\BSON\UTCDateTime();
    $insertOneResult = coleccion("notas")->insertOne($nota);
    return $response->withJson(["status" => ($insertOneResult->getInsertedCount() < 1 ? "ERROR" : "OK")]);
});
