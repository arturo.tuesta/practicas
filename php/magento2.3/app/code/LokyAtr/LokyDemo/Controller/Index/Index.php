<?php

namespace LokyAtr\LokyDemo\Controller\Index;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Event\Observer;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_resultPageFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute() {
        /*
          $textDisplay = new \Magento\Framework\DataObject(array('text' => 'Loky un gran compañero'));
          $this->_eventManager->dispatch('loky_custom_event', ['mensaje' => $textDisplay]);
          //echo $textDisplay->getText();

          $event_data_array = ['cid' => '123'];
          $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
          $this->_eventManager->dispatch('loky_custom_event', $event_data_array);
         */
        $resultPage = $this->_resultPageFactory->create();


        return $resultPage;
    }

}
