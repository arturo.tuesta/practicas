<?php

namespace LokyAtr\LokyDemo\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LokyEscuchandoPersonalizado implements ObserverInterface {

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer) {
        // hacemos cualquier cosa
        // podemos modificar un producto prodemos mandar un mail saraza
        // ponele que hacemos algo aca
        //die('Loky ATR ' . __DIR__) ;
    }

}
