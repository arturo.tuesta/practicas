<?php

namespace LokyAtr\LokyDemo\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LokyEscuchando implements ObserverInterface {

    /**
     * Below is the method that will fire whenever the event runs!
     * @param Observer $observer
     */
    public function execute(Observer $observer) {
        // hacemos cualquier cosa
        // podemos modificar un producto prodemos mandar un mail saraza
        $product = $observer->getProduct();
        $originalName = $product->getName();
        $modifiedName = $originalName . ' - Loky Observer';
        $product->setName($modifiedName);
        $product->save();
    }

}
