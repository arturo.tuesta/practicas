<?php

namespace LokyAtr\LokyDemo\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Model\Product\Interceptor;
use Magento\Framework\App\State;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;

class LokyAtr extends Command {

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var searchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var State
     */
    private $state;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var ConfigLoader
     */
    private $configLoader;

    const NAME = 'name';

    /**
     * Create new update command instance.
     *
     * @param ProductRepositoryInterface $productRepo           [description]
     * @param SearchCriteriaBuilder      $searchCriteriaBuilder [description]
     * @param FilterBuilder              $filterBuilder         [description]
     * @param State                      $state                 [description]
     * @param ObjectManagerInterface     $objectManager         [description]
     * @param Registry                   $registry              [description]
     */
    public function __construct(
            ProductRepositoryInterface $productRepo,
            SearchCriteriaBuilder $searchCriteriaBuilder,
            FilterBuilder $filterBuilder,
            State $state,
            ObjectManagerInterface $objectManager,
            Registry $registry,
            ConfigLoader $configLoader
    ) {
        $this->productRepo = $productRepo;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->state = $state;
        $this->objectManager = $objectManager;
        $this->registry = $registry;
        $this->configLoader = $configLoader;

        parent::__construct();
    }

    protected function configure() {

        $options = [
            new InputOption(
                    self::NAME,
                    null,
                    InputOption::VALUE_REQUIRED,
                    'Name'
            )
        ];

        $this->setName('lokyatr:ejemplo')
                ->setDescription('Es un ejemplo')
                ->setDefinition($options);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        //24-MB01
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$product = $objectManager->create('\Magento\Catalog\Api\Data\ProductInterface');
        //$product->get('24-MB01');

        $this->state->setAreaCode('adminhtml');
        $this->objectManager->configure($this->configLoader->load('adminhtml'));
        $this->registry->register('isSecureArea', true);
        
        $product = $this->productRepo->get('24-MB01');
        
        //print_r($product->getData());
        
        $output->writeln("Producto: " . $product->getName());   
        $output->writeln("Producto Qty: " . $product->getExtensionAttributes()->getStockItem()->getQty());

        $qty = rand(1,200);
        
        $product->setStockData(['qty' => $qty, 'is_in_stock' => 1]);
        $product->setQuantityAndStockStatus(['qty' => $qty, 'is_in_stock' => 1]);
        $product->save();        

//
//
//        $results = $this->productRepo->getList($searchCriteria);
//
//        if ($results->getTotalCount() == 0) {
//            $continue = false;
//            continue;
//        }
//
//        if ($name = $input->getOption(self::NAME)) {
//
//            $output->writeln("Loky dice: Hola " . $name);
//        } else {
//
//            $output->writeln("Mi querido Loky");
//        }

        return $this;
    }

}
