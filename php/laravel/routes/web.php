<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Frontend.Home.index');
});

//Route::resource('pruebas', 'LokyFirebaseDatabaseController');
//Route::resource('apis/notas', 'Apis\NotasController');


Route::group(['middleware' => 'cors', 'prefix' => '/apis/notas/'], function () {
    Route::get('/', 'Apis\NotasController@index');
    Route::post('putData', 'Apis\NotasController@putData');
    Route::delete('deleteData', 'Apis\NotasController@deleteData');
});

Route::get('/firebase-database', 'Frontend\FirebaseDatabaseController@index')->name('firebase-database');;
Route::get('/firebase-firestore', 'Frontend\FirebaseFirestoreController@index')->name('firebase-firestore');;
Route::get('/mongodb', 'Frontend\MongodbController@index')->name('mongodb');;
Route::get('/mongodb-socket', 'Frontend\MongodbSocketController@index')->name('mongodb-socket');;