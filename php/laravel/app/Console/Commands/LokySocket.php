<?php

namespace App\Console\Commands;

use App\Http\Sockets\Notification;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Illuminate\Console\Command;

class LokySocket extends Command {

    private $tiempo_de_espera = 6000;
    private $puerto = 83;
    private $host = '192.168.85.193';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loky:socket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Abrimos un socket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $server = IoServer::factory(new HttpServer(
                                new WsServer(
                                        new Notification()
                                )
                        ), $this->puerto);

        $server->run();
    }

}
