<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use \Illuminate\Http\JsonResponse;

class NotasController extends Controller {

    private function coleccion($coleccion) {
        $uri = "mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky";
        $cliente = new \MongoDB\Client($uri, [],
                [
            'typeMap' => [
                'array' => 'array',
                'document' => 'array',
                'root' => 'array',
            ],
        ]);
        return $cliente->loky->{$coleccion};
    }
    
    /**
     * Return a new JSON response from the application.
     *
     * @param  string|array  $data
     * @param  int  $status
     * @param  array  $headers
     * @param  int  $options
     * @return \Illuminate\Http\JsonResponse
     */
    public function json($data = [], $status = 200, array $headers = [], $options = 0) {
        if ($data instanceof Arrayable && !$data instanceof JsonSerializable) {
            $data = $data->toArray();
        }
        $response = new JsonResponse($data, $status, $headers, $options);
        $response->header("Access-Control-Allow-Origin", "*");
        return $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $options = [
            'sort' => ['timestamp' => -1]
            , 'limit' => 10
        ];
        $filter = [];
        $list = $this->coleccion("notas")->find($filter, $options)->toArray();
        $resultado["hash"] = md5(json_encode($list));
        $resultado["list"] = $list;
        
        return $this->json($resultado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function putData(Request $request) {
        $response = new Response();
        $nota["notaId"] = 1;
        $nota["notaContent"] = $request->get("notaContent", "Nulo");
        $nota["notaUser"] = $request->get("notaUser", "Nulo");
        $nota["timestamp"] =   new \MongoDB\BSON\UTCDateTime();
        $insertOneResult = $this->coleccion("notas")->insertOne($nota);
        return $this->json(["status" => ($insertOneResult->getInsertedCount() < 1 ? "ERROR" : "OK"), "_id" => $insertOneResult->getInsertedId()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteData(Request $request) {
        $response = new Response();
        $notaId = $request->get("notaId");
        $insertOneResult = $this->coleccion("notas")->deleteOne(["_id" => new \MongoDB\BSON\ObjectId($notaId)]);
        return $this->json(["status" => ($insertOneResult->getDeletedCount() < 1 ? "ERROR" : "OK")]);
    }


}
