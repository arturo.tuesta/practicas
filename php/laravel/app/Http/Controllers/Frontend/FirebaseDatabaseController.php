<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller as Controller;

class FirebaseDatabaseController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('Frontend.Genericos.firebasedatabase');
    }
    
}
