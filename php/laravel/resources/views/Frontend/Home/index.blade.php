@extends('Frontend/layout')

@section('body')
<div class="notesBoby">
    <div class="row">
        <div class="col-sm-12">
            <div class="card text-center mt-4">
                <div class="card-body">
                    <a href="{{ route('firebase-database') }}" class="btn btn-info" role="button">
                        Laravel 5.7 firebase (database)
                    </a>
                </div>
            </div>
            <div class="card text-center mt-4">
                <div class="card-body">
                    <a href="{{ route('firebase-firestore') }}" class="btn btn-info" role="button">
                        Laravel 5.7 firebase (firestore)
                    </a>
                </div>
            </div>
            <div class="card text-center mt-4">
                <div class="card-body">
                    <a href="{{ route('mongodb') }}" class="btn btn-info" role="button">
                        Laravel 5.7 mongodb api
                    </a>
                </div>
            </div>
            <div class="card text-center mt-4">
                <div class="card-body">
                    <a href="{{ route('mongodb-socket') }}" class="btn btn-info" role="button">
                        Laravel 5.7 mongodb (socket)
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection