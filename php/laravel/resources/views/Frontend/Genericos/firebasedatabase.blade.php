@extends('Frontend/layout')

@section('body')
    <div class="notesHeader">
        <h5 class="text-white">Pruebas con Laravel 5.7 y firebase(database)</h5>
    </div>
    <div class="notesBoby">
        <div class="notesBoby">
            <div class="row">
                <div class="col-sm-4">
                    <form class="text-center border border-light p-2">
                        <div class="form-group">
                            <input type="text"class="form-control" name="mensaje" id="mensaje" aria-describedby="emailHelp" placeholder="Ingresa un mensaje" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="usuario" id="usuario" aria-describedby="emailHelp" placeholder="Ingresa tu nombre" />
                        </div>
                        <button type="submit" onClick="agregarNota(event)" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
                <div class="col-sm-8">
                    <div id='listadodenotas'>
                        <div class="lds-ripple centered"><div></div><div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://www.gstatic.com/firebasejs/5.7.3/firebase-app.js"></script> 
    <script src="https://www.gstatic.com/firebasejs/5.7.3/firebase-database.js"></script>
    <script>
        var config = {
            apiKey: "AIzaSyAK0UyNSm8v1oxydXsB51F1vN1WCAjmUQw",
            authDomain: "lokydemo.firebaseapp.com",
            databaseURL: "https://lokydemo.firebaseio.com",
            projectId: "lokydemo",
            storageBucket: "lokydemo.appspot.com",
            messagingSenderId: "1082926390774"
        };
        firebase.initializeApp(config);
        
        var database = firebase.database();

        var firebaseNotasCollection = database.ref().child('notas');

        function agregarNota(event) {
            event.preventDefault();
            
            if ($('#mensaje').val() != "" && $('#usuario').val() != "") {
                var nota = {
                    notaContent: $('#mensaje').val(),
                    notaUser: $('#usuario').val(), //another way you could write is $('#myForm [name="fullname"]').
                };
                if ($('#usuario').val() !== "") {
                    $('#usuario').prop('disabled', true);
                }
                $('#mensaje').val("");
                $('#mensaje').focus();
                firebaseNotasCollection.push(nota).key;
            }

        }

        function eliminarNota(notaId) {
            firebaseNotasCollection.child(notaId).remove();
        }

        firebaseNotasCollection.on('value', function (notas) {
            firebaseNotasCollection.orderByChild("timestamp").on('child_removed', snap => {
                for (i = 0;
                        i < notas.length;
                        i++
                        ) {
                    if (notas[i].notaId == snap.key) {
                        notas.splice(i, 1)
                    }
                }
            }
            )

            var newNotas = [];
            notas.forEach(function (firebaseOrderReference) {
                var nota = firebaseOrderReference.val();
                newNotas.push({
                    notaKey: firebaseOrderReference.key,
                    notaContent: nota.notaContent,
                    notaUser: nota.notaUser
                })
            });

            var allOrdersHtml = "";
            newNotas.reverse();
            newNotas.map((nota, key) => {
                var individialOrderHtml = `<div class="card text-center mt-4">
                    <div class="card-header">
                    usuario: ` + nota.notaUser + `
                    </div>
                    <div class="card-body">
                    <h5 class="card-title">` + nota.notaContent + `</h5>
                    <button type="button" onclick="eliminarNota('${nota.notaKey}')" class="btn btn-primary">Eliminar</button>
                    </div>
                    </div>
                    `;
                allOrdersHtml += individialOrderHtml;
            });

            $('#listadodenotas').html(allOrdersHtml);

        });

    </script>
@endsection