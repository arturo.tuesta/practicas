@extends('Frontend/layout')

@section('body')
    <div class="notesHeader">
        <h5 class="text-white">Pruebas con Laravel 5.7 y firebase(firestore)</h5>
    </div>
    <div class="notesBoby">
        <div class="notesBoby">
            <div class="row">
                <div class="col-sm-4">
                    <form class="text-center border border-light p-2">
                        <div class="form-group">
                            <input type="text"class="form-control" name="mensaje" id="mensaje" aria-describedby="emailHelp" placeholder="Ingresa un mensaje" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="usuario" id="usuario" aria-describedby="emailHelp" placeholder="Ingresa tu nombre" />
                        </div>
                        <button type="submit"  onClick="agregarNota(event)" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
                <div class="col-sm-8">
                    <div id='listadodenotas'>
                        <div class="lds-ripple centered"><div></div><div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://www.gstatic.com/firebasejs/5.7.3/firebase-app.js"></script> 
    <script src="https://www.gstatic.com/firebasejs/5.7.3/firebase-firestore.js"></script>
    <script>
        var config = {
            apiKey: "AIzaSyAK0UyNSm8v1oxydXsB51F1vN1WCAjmUQw",
            authDomain: "lokydemo.firebaseapp.com",
            databaseURL: "https://lokydemo.firebaseio.com",
            projectId: "lokydemo",
            storageBucket: "lokydemo.appspot.com",
            messagingSenderId: "1082926390774"
        };
        firebase.initializeApp(config);

        var database = firebase.firestore();
        database.settings({timestampsInSnapshots: true});
        var firebaseNotasCollection = database.collection('notas');

        var onCollectionUpdate = (querySnapshot) => {

            var notas = [];
            querySnapshot.forEach((doc) => {
                var {notaContent, notaUser} = doc.data();
                notas.unshift({
                    key: doc.id,
                    doc, // DocumentSnapshot
                    notaId: doc.id,
                    notaContent: notaContent,
                    notaUser: notaUser
                });
            });
            generarListado(notas.reverse());
        }

        firebaseNotasCollection.orderBy("timestamp", "desc").onSnapshot(onCollectionUpdate);

        function agregarNota(event) {
            event.preventDefault();
            if ($('#mensaje').val() != "" && $('#usuario').val() != "") {
                var nota = {
                    notaContent: $('#mensaje').val(),
                    notaUser: $('#usuario').val(), //another way you could write is $('#myForm [name="fullname"]').
                };
                if ($('#usuario').val() !== "") {
                    $('#usuario').prop('disabled', true);
                }
                firebaseNotasCollection.add({
                    notaId: 0,
                    notaContent: $('#mensaje').val(),
                    notaUser: $('#usuario').val(),
                    timestamp: firebase.firestore.FieldValue.serverTimestamp()
                }).then((docRef) => {
                    console.log("Agregado correctamente");
                })
                        .catch((error) => {
                            console.error("Error adding document: ", error);
                        });
                $('#mensaje').val("");
                $('#mensaje').focus();
            }
        }

        function generarListado(notas) {
            var allOrdersHtml = "";
            notas.map((nota, key) => {
                var individialOrderHtml = `<div class="card text-center mt-4">
         <div class="card-header">
         usuario: ` + nota.notaUser + `
         </div>
         <div class="card-body">
         <h5 class="card-title">` + nota.notaContent + `</h5>
         <button type="button" onclick="eliminarNota('${nota.key}')" class="btn btn-primary">Eliminar</button>
         </div>
         </div>
         `;
                allOrdersHtml += individialOrderHtml;
            });

            $('#listadodenotas').html(allOrdersHtml);
        }

        function eliminarNota(notaId) {
            firebaseNotasCollection.doc(notaId).delete().then(() => {
              console.log("Documento eliminado correctamente");
            }).catch((error) => {
              console.error("Error removing document: ", error);
            });
        }
    </script>
@endsection