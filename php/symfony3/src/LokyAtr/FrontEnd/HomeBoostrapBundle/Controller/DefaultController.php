<?php

namespace LokyAtr\FrontEnd\HomeBoostrapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('@LokyAtrFrontEndHomeBoostrap/Default/index.html.twig');
    }
}
