<?php

namespace LokySocketBundle\Command;

use Symfony\Component\Console\Command\Command;
use LokySocketBundle\Sockets\Notification;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SocketCommand extends ContainerAwareCommand {

    protected static $defaultName = 'lokysocket:prueba';
    private $tiempo_de_espera = 6000;
    private $puerto = 83;
    private $host = '192.168.85.193';

    protected function configure() {
        $this
                ->setName('lokysocket:prueba')
                ->setHelp('Aca va la ayuda')
                ->setDescription('Probar socket.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {


        $server = IoServer::factory(new HttpServer(
                                new WsServer(
                                        new Notification($this->getContainer())
                                )
                        ), $this->puerto);

        $server->run();
    }

}
