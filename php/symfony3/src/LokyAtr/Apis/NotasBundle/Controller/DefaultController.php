<?php

namespace LokyAtr\Apis\NotasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use MongoDB\BSON\UTCDateTime;
class DefaultController extends Controller {

    private function coleccion($coleccion) {
        $uri = "mongodb://arturo:miqueridololy2016@ds255784.mlab.com:55784/loky";
        $cliente = new \MongoDB\Client($uri, [],
                [
            'typeMap' => [
                'array' => 'array',
                'document' => 'array',
                'root' => 'array',
            ],
        ]);
        return $cliente->loky->{$coleccion};
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function indexAction() {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $options = [
            'sort' => ['timestamp' => -1]
            , 'limit' => 10
        ];
        $filter = [];
        $list = $this->coleccion("notas")->find($filter, $options)->toArray();
        $resultado["hash"] = md5(json_encode($list));
        $resultado["list"] = $list;
        
        $response->setContent(json_encode($resultado, JSON_PRETTY_PRINT));
        return $response;
    }

    /**
     * @Route("/putData", methods="post")
     */
    public function putDataAction(Request $request) {
        $response = new Response();
        $nota["notaId"] = 1;
        $nota["notaContent"] = $request->get("notaContent", "Nulo");
        $nota["notaUser"] = $request->get("notaUser", "Nulo");
        $nota["timestamp"] =   new \MongoDB\BSON\UTCDateTime();
        $insertOneResult = $this->coleccion("notas")->insertOne($nota);
        $response->setContent(json_encode(["status" => ($insertOneResult->getInsertedCount() < 1 ? "ERROR" : "OK"), "_id" => $insertOneResult->getInsertedId()], JSON_PRETTY_PRINT));
        return $response;
    }

    /**
     * @Route("deleteData", methods="delete")
     */
    public function deleteDataAction(Request $request) {
        $response = new Response();
        $notaId = $request->get("notaId");
        $insertOneResult = $this->coleccion("notas")->deleteOne(["_id" => new \MongoDB\BSON\ObjectId($notaId)]);
        $response->setContent(json_encode(["status" => ($insertOneResult->getDeletedCount() < 1 ? "ERROR" : "OK")], JSON_PRETTY_PRINT));
        return $response;
    }

}
