import { Component, OnInit } from '@angular/core';
import { RestApiLoky } from '../rest-api.service';

@Component({
  selector: 'app-pwa',
  templateUrl: './pwa.page.html',
  styleUrls: ['./pwa.page.scss'],
})

export class PwaPage implements OnInit  {

  users: any[] = [];

  constructor(
    public restApiLoky: RestApiLoky
  ) {}

  ngOnInit() {
    this.restApiLoky.obtenerContenidos()
    .subscribe(
      (data) => {
        this.users = data['data'];
      },
      (error) =>{
        console.error(error);
      }
    )
  }
}
