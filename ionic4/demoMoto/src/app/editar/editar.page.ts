import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RestApiLoky } from '../rest-api.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})

export class EditarPage implements OnInit {

  item: any;
  edit_item_form: FormGroup;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      public formBuilder: FormBuilder,
      private restApiLoky: RestApiLoky
  ) { }

  ngOnInit() {
    var _this = this;
    this.route.params.subscribe(
      data => {
        this.restApiLoky.obtenerContenidoPorId(data.id)
        .subscribe(
          (data) => {
            _this.item = data["data"];
            if(!_this.item){
              this.volvemos();
            } else{
              _this.item = _this.item;
              _this.edit_item_form = _this.formBuilder.group({
                nombre: new FormControl(_this.item.nombre, Validators.required),
                modelo: new FormControl(_this.item.modelo, Validators.required),
                color: new FormControl(_this.item.color, Validators.required),
                cilindrada: new FormControl(_this.item.cilindrada, Validators.required),
                precio: new FormControl(_this.item.precio, Validators.required)
              });
            }
          },
          (error) =>{
            console.error(error);
          }
        )
      }
    )
  }

  volvemos(){
    this.router.navigate(['/loky']);
  }

  editarContenido(value){
    var id = this.item._id;
    this.restApiLoky.editarContenido(id, value);
    this.edit_item_form.reset();
    this.volvemos();
  }

}
