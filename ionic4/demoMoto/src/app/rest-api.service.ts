import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RestApiLoky {


  constructor(
    private http: HttpClient
  ) {}

  obtenerContenidos() {
    return this.http.get("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/getData');
  }

  agregarContenido(value){
    let guardar = {
      nombre: value.nombre,
      modelo: value.modelo,
      color: value.color,
      cilindrada: value.cilindrada,
      precio: value.precio,
      timestamp: new Date().getTime()
    }
    let data_send  = JSON.stringify(guardar);
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        this.http.post("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/putData',data_send, {headers: {'Content-Type': 'application/json'}})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  editarContenido(id, value){
    let guardar = {
      id: id,
      update: {
        nombre: value.nombre,
        modelo: value.modelo,
        color: value.color,
        cilindrada: value.cilindrada,
        precio: value.precio
      }
    }

    var data_send = JSON.stringify(guardar);
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        this.http.post("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/updateData',data_send, {headers: {'Content-Type': 'application/json'}})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  eliminarContenidoPorId(id){
    return this.http.get("https://shrouded-taiga-92727.herokuapp.com/backend/motos/" + 'eliminar/' + id);
  }

  obtenerContenidoPorId(id){
    return this.http.get("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/getData/' + id);
  }
}
