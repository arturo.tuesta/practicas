import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class RestApiLoky {

  connected: null;
  disconnected: null;

  constructor(
    private http: HttpClient
    , private storage: Storage
  ) {
  }

  obtenerContenidos() {
    return new Promise((resolve, reject) => {
        if (this.hayConexion()) {
            this.http.get("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/getData')
            .subscribe(data => {
                this.asignarStorage(data);
                resolve(data);
            })
        }
        else{
            this.storage.get('lokyLista').then((val) => {
                if(val !== null){
                    resolve(val);
                }
                else{
                    reject("No hay datos");
                }
            });
        }
    });
  }

  agregarContenido(value){
    let guardar = {
      nombre: value.nombre,
      modelo: value.modelo,
      color: value.color,
      cilindrada: value.cilindrada,
      precio: value.precio,
      timestamp: new Date().getTime()
    }
    let data_send  = JSON.stringify(guardar);
    return new Promise((resolve, reject) => {
        if (this.hayConexion()) {
            let headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            this.http.post("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/putData',data_send, {headers: {'Content-Type': 'application/json'}})
            .subscribe(res => {
              resolve(res);
            }, (err) => {
                this.apilarPendientesStorage('lokyagregar', guardar)
                .then((val) => {
                  reject("Agregar se guardo en cache");
                });
            });
        }
        else{
            this.apilarPendientesStorage('lokyagregar', guardar)
            .then((val) => {
              resolve("Agregar se guardo en cache");
            });
        }

    });
  }

  editarContenido(id, value){
    let guardar = {
      id: id,
      update: {
        nombre: value.nombre,
        modelo: value.modelo,
        color: value.color,
        cilindrada: value.cilindrada,
        precio: value.precio
      }
    }

    var data_send = JSON.stringify(guardar);
    return new Promise((resolve, reject) => {
        if (this.hayConexion()) {
            let headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json');
            this.http.post("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/updateData',data_send, {headers: {'Content-Type': 'application/json'}})
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              this.apilarPendientesStorage('lokyeditar', guardar);
              reject(err);
            });
        }
        else{
            this.apilarPendientesStorage('lokyeditar', guardar);
            reject("Editar se guardo en cache");
        }
    });
  }

  eliminarContenidoPorId(id){
    return new Promise((resolve, reject) => {
        if (this.hayConexion()) {
            this.http.get("https://shrouded-taiga-92727.herokuapp.com/backend/motos/" + 'eliminar/' + id)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              this.apilarPendientesStorage('lokyeliminar', {id: id})
              reject(err);
            });
        }
        else{
            this.apilarPendientesStorage('lokyeliminar', {id: id}) 
            reject("Eliminar se guardo en cache");          
        }
    });
    
  }

  obtenerContenidoPorId(identificador){
    return new Promise((resolve, reject) => {
        if (this.hayConexion()) {
            this.http.get("https://shrouded-taiga-92727.herokuapp.com/apis/motos" + '/getData/' + identificador)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err);
            });
        }
        else{
            this.storage.get('lokyLista').then((val) => {
                let buscamos = val['data'].find((row) => {
                    return row._id === identificador;
                });
                if(buscamos !== null){
                    resolve({data: buscamos});
                }
                else{
                    reject("no hay datos");
                }
            });
        }
    });
  }

  apilarPendientesStorage(key, datos)  {
    return new Promise((resolve, reject) => {
        this.storage.get(key).then((val) => {
            var pendientes = [];
            if(val !== null){
                pendientes = val;
            }
            if(key === "lokyeliminar"){
                this.storage.get('lokyLista').then((lista) => {
                    if(lista.data && lista.data.length){
                        console.log("AAAAA");
                        console.log(datos.id);
                        const result = lista.data.filter(de => de._id !== datos.id);
                        var _this = this;
                        this.eliminacionContenido(result).then((data) =>{
                            pendientes.push({datos});
                            resolve(_this.storage.set(key, pendientes));
                        });
                    }
                });
            }
            else{
                pendientes.push({datos});
                resolve(this.storage.set(key, pendientes));
            }
        });
    });
  }     

  eliminacionContenido(result){
    return new Promise((resolve, reject) => {
        resolve(this.asignarStorage({data: result}))
    });
  }

  asignarStorage(valor){
    if(valor !== null){
        return this.storage.set('lokyLista', valor);
    }
  }

  hayConexion(){
    if(navigator.onLine){
        return true;
    }
    else{
        return false;
    }
  }

}
