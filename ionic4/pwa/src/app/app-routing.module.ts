import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'arturotuesta', pathMatch: 'full' },
  { path: 'arturotuesta', loadChildren: './home/home.module#HomePageModule' },
  { path: 'loky', loadChildren: './pwa/pwa.module#PwaPageModule'  },
  { path: 'loky/agregar', loadChildren: './agregar/agregar.module#AgregarPageModule' },
  { path: 'loky/editar/:id', loadChildren: './editar/editar.module#EditarPageModule' },
  { path: 'loky/eliminar/:id', loadChildren: './eliminar/eliminar.module#EliminarPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
