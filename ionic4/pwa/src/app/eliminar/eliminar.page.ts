import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { RestApiLoky } from '../rest-api.service';
@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.page.html',
  styleUrls: ['./eliminar.page.scss'],
})
export class EliminarPage implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private restApiLoky: RestApiLoky
  ) { }

  ngOnInit() {
    var _this = this;
    this.route.params.subscribe(
      data => {
        _this.restApiLoky.eliminarContenidoPorId(data.id);
        _this.volvemos();
      }
    )
  }

  volvemos(){
    this.router.navigate(['/loky']);
  }

}
