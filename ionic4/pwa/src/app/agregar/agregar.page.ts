import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RestApiLoky } from '../rest-api.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  new_item_form: FormGroup;

  constructor(
      private router: Router,
      public formBuilder: FormBuilder,
      private restApiLoky: RestApiLoky
  ) { }

  ngOnInit() {
    this.new_item_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.required),
      modelo: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      cilindrada: new FormControl('', Validators.required),
      precio: new FormControl('', Validators.required)
    });
  }

  volvemos(){
    this.router.navigate(['/loky']);
  }

  agregarContenido(value){
    this.restApiLoky.agregarContenido(value);
    this.new_item_form.reset();
    this.volvemos();
  }

}
