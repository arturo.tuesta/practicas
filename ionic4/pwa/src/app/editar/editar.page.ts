import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RestApiLoky } from '../rest-api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})

export class EditarPage implements OnInit {

  item: any;
  edit_item_form: FormGroup;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      public formBuilder: FormBuilder,
      private restApiLoky: RestApiLoky, 
      private storage: Storage
  ) { }

  ngOnInit() {
    var _this = this;
    this.route.params.subscribe(
      data => {
        const identificador = data.id;
        this.restApiLoky.obtenerContenidoPorId(identificador)
        .then(
          (data) => {
            _this.item = data["data"];
            if(!_this.item){
              this.volvemos();
            } else{
              _this.asignarDatosFormulario(_this.item, _this);
            }
          },
          (error) =>{
            this.storage.get('lokyLista').then((val) => {
                let buscamos = val.find((row) => {
                    return row._id === identificador;
                });
                _this.asignarDatosFormulario(buscamos, _this);
            });
            console.error(error);
          }
        )
      }
    )
  }

  asignarDatosFormulario(item, ts){
    ts.edit_item_form = ts.formBuilder.group({
      nombre: new FormControl(item.nombre, Validators.required),
      modelo: new FormControl(item.modelo, Validators.required),
      color: new FormControl(item.color, Validators.required),
      cilindrada: new FormControl(item.cilindrada, Validators.required),
      precio: new FormControl(item.precio, Validators.required)
    });
  }

  volvemos(){
    this.router.navigate(['/loky']);
  }

  editarContenido(value){
    var id = this.item._id;
    var _this = this;
    this.restApiLoky.editarContenido(id, value)
    .then((data) => {
        _this.edit_item_form.reset();
        _this.volvemos();
    });
  }

  buscarEnLocal(){
    
  }

}
