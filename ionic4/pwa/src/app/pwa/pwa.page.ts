import { Component, OnInit } from '@angular/core';
import { RestApiLoky } from '../rest-api.service';
import { Storage } from '@ionic/storage';
    
@Component({
  selector: 'app-pwa',
  templateUrl: './pwa.page.html',
  styleUrls: ['./pwa.page.scss'],
})

export class PwaPage implements OnInit  {

  users: any[] = [];

  constructor(
    public restApiLoky: RestApiLoky
    , private storage: Storage
  ) {}

  ionViewDidLoad(){
    this.restApiLoky.obtenerContenidos()
    .then(data => {
        this.users = data['data'];
    });
      console.log("ioooo");
  }

  ngOnInit() {
    this.restApiLoky.obtenerContenidos()
    .then(data => {
        this.users = data['data'];
    });
  }
    
  asignarStorage (valor){
    if(valor !== null){
        console.log("Entro a guardar");
        this.storage.set('lokyLista', valor);
        this.storage.set('lokyLista', valor);
    }
  }
}
