import { Component } from '@angular/core';
 
@Component({
  selector: 'loky',
  template: `<h3>Mi Loky querido!</h3>
    <img [src] = "imagenLoky" width="200" />
    <img src = "{{ imagenLoky }}" width="200"  />
    <button [disabled] = "botonDisabledStatus" (click) = "eventroClick($event)" >Mi boton</button>
    <input type = "text" (keyup) = "tecladoArriba($event)" />
    <input type = "text" (keyup.enter) = "enterTocado()" />
    <input type = "text" (keyup.enter) = "dimeElNombre(loky.value)" #loky />
    `,
  styles: ['']
})
export class LokyComponent {
  title = 'Loky';
  imagenLoky = "assets/loky.jpeg";
  botonDisabledStatus = false;
   
  eventroClick(e){
    console.log(e);  
  }
  
  tecladoArriba(e){
    console.log("Texto ingresado: " + e.target.value);
  }

  enterTocado(){
    console.log("tecla enter arriba Loky!");
  }
  
  dimeElNombre (nombre){
    console.log(nombre);
  }
}
