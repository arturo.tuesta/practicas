import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lokytpldatos',
  templateUrl: './lokytpldatos.component.html',
  styles: ['']
})
export class LokytpldatosComponent implements OnInit {
   
  @Input() datospersonales;

  constructor(){
  }

  ngOnInit(){
  }

  loky = {
        nombre: "Arturo",
        apellido: "Tuesta"
    }
}
